import { authHeader, handleResponse } from "./auth.service";

const apiUrl = process.env.VUE_APP_API_URL;

export const dataService = {
  getPersonalStatistics,
  getAllCourses,
  getCourseStatistics,
  getAllEvents,
  getCourseData,
  getEventTypes,
  createEvent,
  getNewLink,
  checkInOnEvent,
  checkinManually,
  getEventStatistics,
};

function getCachedVersion(key) {
  try {
    cache = JSON.parse(localStorage.getItem(key));
    if (cache && cache.endsAt < new Date().getTime()) {
      cache = null;
      localStorage.removeItem(key);
    }
    return cache;
  } catch (e) {
    console.error(e);
  }
}

function storeCachedVersion(key, value) {
  try {
    if (typeof value === "object") {
      const date = new Date();
      const minutes = date.getMinutes() + 5;
      date.setMinutes(minutes % 60);
      if (minutes >= 60) {
        date.setHours(date.getHours() + 1);
      }
      value.endsAt = date.getTime();
    }
    if (value) localStorage.setItem(key, JSON.stringify(value));
    return value;
  } catch (e) {
    console.error(e);
  }
}

function getPersonalStatistics(userId, isOwn = false) {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };
  return fetch(
    !isOwn
      ? `${apiUrl}/statistic/personal_statistic/${userId}/`
      : `${apiUrl}/statistic/own_statistic/`,
    requestOptions
  ).then(handleResponse);
}

function getEventStatistics(eventId) {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };
  return fetch(`${apiUrl}/statistic/event_statistic/${eventId}/`, requestOptions).then(handleResponse);
}

let cache = null;

function getAllCourses() {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };

  cache = getCachedVersion("courses");
  return cache
    ? new Promise(resolve => resolve(cache))
    : fetch(`${apiUrl}/education/course/`, requestOptions)
        .then(handleResponse)
        .then(courses => storeCachedVersion(`courses`, courses));
}

function getCourseData(id) {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };

  return fetch(`${apiUrl}/education/course/${id}/`, requestOptions).then(handleResponse);
}

function getEventTypes() {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };

  cache = getCachedVersion("eventTypes");
  return cache
    ? new Promise(resolve => resolve(cache))
    : fetch(`${apiUrl}/attendance/event-types/`, requestOptions)
        .then(handleResponse)
        .then(eventTypes => storeCachedVersion(`eventTypes`, eventTypes));
}

function getCourseStatistics(courseId, isOwn = false) {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };
  return fetch(
    !isOwn
      ? `${apiUrl}/statistic/course_statistic/${courseId}/`
      : `${apiUrl}/statistic/own_statistic/${courseId}/`,
    requestOptions
  )
    .then(handleResponse)
    .then(course => storeCachedVersion(`course_${courseId}`, course));
}

function getAllEvents() {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };

  cache = getCachedVersion("events");
  return cache
    ? new Promise(resolve => resolve(cache))
    : fetch(`${apiUrl}/attendance/event/`, requestOptions)
        .then(handleResponse)
        .then(courses => storeCachedVersion(`events`, courses));
}

function createEvent(event) {
  const requestOptions = {
    method: "POST",
    headers: { ...authHeader(), "Content-Type": "application/json" },
    body: JSON.stringify(event)
  };

  return fetch(`${apiUrl}/attendance/event/`, requestOptions).then(
    handleResponse
  );
}

function getNewLink(uuid) {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };
  return fetch(
    `${apiUrl}/attendance/check-in-url/${uuid}/`,
    requestOptions
  ).then(handleResponse);
}

function checkInOnEvent(token) {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };
  return fetch(`${apiUrl}/attendance/check-in/${token}/`, requestOptions);
}

function checkinManually(student, event, action) {
  const requestOptions = {
    method: "POST",
    headers: { ...authHeader(), "Content-Type": "application/json" },
    body: JSON.stringify({ student, event, action: action ? 1 : 0 })
  };

  return fetch(`${apiUrl}/attendance/event/manual/`, requestOptions).then(
      handleResponse
  );
}
