import { authHeader, handleResponse } from "./auth.service";

const apiUrl = process.env.VUE_APP_API_URL;

export const userService = {
  login,
  logout,
  getAll: getMyProfile,
  getById: getUserById,
  getMyProfile: getMyProfile,
  changePassword
};

function login(username, password) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ username, password })
  };

  return fetch(`${apiUrl}/user/login/`, requestOptions)
    .then(handleResponse)
    .then(response => {
      if (response && response.token) {
        localStorage.setItem("auth_token", JSON.stringify(response.token));
      }
      return response.token;
    });
}

function logout() {
  localStorage.clear();
}

function getMyProfile() {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };

  return fetch(`${apiUrl}/user/profile/`, requestOptions)
    .then(handleResponse)
    .then(profile => {
      if (profile) {
        localStorage.setItem("user", JSON.stringify(profile));
      }
      return profile;
    });
}

function getUserById(id) {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };

  return fetch(`${apiUrl}/user/profile/${id}/`, requestOptions).then(
    handleResponse
  );
}

function changePassword(oldPassword, newPassword) {
  const requestOptions = {
    method: "POST",
    headers: {
      ...authHeader(),
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      old_password: oldPassword,
      new_password1: newPassword,
      new_password2: newPassword
    })
  };
  console.log(oldPassword, newPassword);

  return fetch(`${apiUrl}/user/password/change/`, requestOptions)
    .then(handleResponse)
    .then(profile => {
      if (profile) {
        localStorage.setItem("user", JSON.stringify(profile));
      }
      return profile;
    });
}
