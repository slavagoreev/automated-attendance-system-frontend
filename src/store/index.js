import Vue from "vue";
import Vuex from "vuex";

import { users } from "./users";
import { account } from "./account";
import { alert } from "./alert";

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    users,
    account,
    alert
  }
});
