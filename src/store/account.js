import { userService } from "../services";
import { router } from "../router";

const user = localStorage.getItem("user");
const state = user
  ? { status: { loggedIn: true }, user: JSON.parse(user) }
  : { status: {}, user: null };

const actions = {
  async login({ dispatch, commit }, { username, password }) {
    commit("loginRequest", { username });
    try {
      const token = await userService.login(username, password);
      commit("loginSuccess", token);
      router.push("/");
      return true;
    } catch (e) {
      commit("loginFailure", "Login failed!");
      dispatch("alert/error", "Login failed!", { root: true });
      setTimeout(() => {
        dispatch("alert/clear", null, { root: true });
      }, 5000);
      return false;
    }
  },
  async changePassword({ dispatch, commit }, { oldPassword, newPassword }) {
    commit("changePasswordRequest");
    try {
      await userService.changePassword(oldPassword, newPassword);
      commit("changePasswordSuccess");
      dispatch("alert/success", "Password changed successfully!", { root: true });
      return true;
    } catch (e) {
      commit("changePasswordFailure", "Change password failed!");
      dispatch("alert/error", "Change password failed!", { root: true });
      setTimeout(() => {
        dispatch("alert/clear", null, { root: true });
      }, 5000);
      return false;
    }
  },

  async getMyProfile({ commit }) {
    commit("getMyProfileRequest");
    try {
      const profile = await userService.getMyProfile();
      commit("getMyProfileSuccess", profile);
    } catch (e) {
      commit("getMyProfileFailure", "Couldn't fetch the profile");
    }
  },

  logout({ commit }) {
    userService.logout();
    commit("logout");
  }
};

const mutations = {
  getMyProfileRequest(state) {
    state.loading = true;
  },
  getMyProfileSuccess(state, profile) {
    state = { ...state, loading: false, profile };
  },
  getMyProfileFailure(state, error) {
    state = { ...state, error, profile: null };
  },
  loginRequest(state) {
    state.status = { loggingIn: true };
  },
  loginSuccess(state) {
    state.status = { loggedIn: false };
  },
  loginFailure(state) {
    state.status = {};
    state.user = null;
  },
  changePasswordRequest(state) {
    state.status = { changingPassword: true };
  },
  changePasswordSuccess(state) {
    state.status = { changingPassword: false };
  },
  changePasswordFailure(state) {
    state.status = {};
    state.user = null;
  },
  logout(state) {
    state.status = {};
    state.user = null;
  }
};

export const account = {
  namespaced: true,
  state,
  actions,
  mutations
};
