import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    { path: "/", name: "home", component: Home },
    {
      path: "/login",
      name: "login",
      component: () =>
        import(/* webpackChunkName: "login" */ "./views/User/Login.vue")
    },
    {
      path: "/courses",
      name: "courses",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/Courses.vue")
    },
    {
      path: "/course/:id",
      name: "course-statistics",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/CourseStatistics.vue")
    },
    {
      path: "/student/:id",
      name: "student-statistics",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/StudentStatistics.vue")
    },
    {
      path: "/profile",
      name: "profile",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/User/Profile.vue")
    },
    {
      path: "/settings",
      name: "settings",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/User/Settings.vue")
    },
    {
      path: "/create-event",
      name: "create-event",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/Event/CreateEvent.vue")
    },
    {
      path: "/list-event",
      name: "list-event",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/Event/ListEvent.vue")
    },
    {
      path: "/event/:id",
      name: "event",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/Event/Event.vue")
    },
    {
      path: "/check-in-session/:id/:uuid",
      name: "check-in-session",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/Event/CheckInSession.vue")
    },
    {
      path: "/check-in/:token",
      name: "check-in",
      component: () =>
        import(/* webpackChunkName: "course-statistics" */ "./views/Event/CheckIn.vue")
    },
    { path: "*", redirect: "/" }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ["/login"];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem("auth_token");

  if (authRequired && !loggedIn) {
    return next("/login");
  }

  next();
});
